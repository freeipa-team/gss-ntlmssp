gss-ntlmssp (1.2.0-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 16 Feb 2023 14:16:33 -0000

gss-ntlmssp (1.2.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #1031369)
    - CVE-2023-25563
    - CVE-2023-25564
    - CVE-2023-25565
    - CVE-2023-25566
    - CVE-2023-25567
  * watch: Updated.
  * source: Add files to extend-diff-ignore.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 16 Feb 2023 15:55:07 +0200

gss-ntlmssp (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Update upstream urls.
  * add-openssl-1.1.0-compat.diff: Dropped, upstream.
  * rules: Drop unwanted docs.
  * source: Switch format to 3.0 (quilt). (Closes: #1007285)
  * control, rules: Migrate to debhelper-compat, bump to 13.
  * control: Bump policy to 4.6.0.
  * control: Update vcs urls.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 15 Mar 2022 15:37:42 +0200

gss-ntlmssp (0.7.0-4) unstable; urgency=medium

  * control, copyright, watch: Update urls.
  * compat, control, rules: Migrate to debhelper 10.
  * control: Bump policy to 4.1.0, no changes.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 21 Sep 2017 15:10:22 +0300

gss-ntlmssp (0.7.0-3) unstable; urgency=medium

  * Install mech config file renamed so that it's actually used, and
    remove the old one on upgrade. Thanks, Steve Langasek!
    (Closes: #845399)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 23 Nov 2016 10:18:53 +0200

gss-ntlmssp (0.7.0-2) unstable; urgency=medium

  * rules: Disable tests until failure on big endian architectures
    has been resolved upstream. (Closes: #845080)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 22 Nov 2016 17:45:44 +0200

gss-ntlmssp (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * rules, control: Use quilt, run ntlmssptest.
  * add-openssl-1.1.0-compat.diff: Add support for openssl 1.1.0.
    (Closes: #828334)
  * control: Bump policy to 3.9.8, no changes.
  * control: Use https VCS urls.
  * control: Add zlib1g-dev to build-depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 19 Sep 2016 15:20:38 +0300

gss-ntlmssp (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: #795647)

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 16 Aug 2015 03:27:03 +0300
